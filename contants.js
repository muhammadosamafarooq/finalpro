module.exports = {
    HTTP: {
        CODES: {
            SUCCESS: 200,
            UPDATE: 201,
            BAD_REQUEST: 400,
            UNAUTHORIZED: 403,
            SERVER_ERROR: 500,
            NOTFOUND : 404,
        },
    },

    MESSAGES:{
        LOGIN:{
            SUCCESS:"You have successfully logged in.",
            AUTH_FAILED: "User Authentication Failed!",
            INACTIVE: "Your account is deactivated, please login again."
        },
        FORBIDDEN:"Access Forbidden",
        GENERAL:{
            FIELDS_REQUIRED: "Input fields are missing or invalid, please provide the correct required fields."
        },
        CREATE: {
            SUCCESS: "Added Succesfully",
            FAILURE: "Failed to add",
            NOTFOUND:"Not Found",
            ALREADY:"Already Exist"
        },
        DELETE:{
            SUCCESS:"Deleted Succesfully",
            FAILURE:"Failed to delete",
            NOTFOUND:"Not Found"
        },
        UPDATE:{
            SUCCESS:"Updated Succesfully",
            FAILURE:"Failed to update",
            NOTFOUND:"Not Found"
        },
        ERROR: {
           ERRORUPDATE : "error update ",
            ERRORFIND: " error server ",
            ERRORDELETE: "error delete",
            ERRORCREATE : "error create"
        },
        FIND:{
            SUCCESS:"Found Succesfully",
            FAILURE:"Failed to find",
            NOTFOUND:"Not Found"
        },

    },
    dirName:"F:/notes/7_sem/MeanStack/IAD_Express/AIH_SMS_Server"
}/**
 * Created by Idrees on 4/19/2017.
 */
