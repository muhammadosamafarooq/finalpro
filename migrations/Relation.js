'use strict';
module.exports = {
    up: function (queryInterface, Sequelize) {
        return ([queryInterface.addColumn('students','studSectionId' ,{

                type: Sequelize.INTEGER,
            onDelete:'CASCADE',
                references:{
                    model:'sections',
                    key:'id',
                    as:'studSectionId',
                }


        }),queryInterface.addColumn('courses','crTeacherId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'teachers',
                key:'id',
                as:'crTeacherId',
            }

        }),queryInterface.addColumn('campuses','adminId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'admins',
                key:'id',
                as:'adminId',
            }

        }),queryInterface.addColumn('sections','secTeacherId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'teachers',
                key:'id',
                as:'secTeacherId',
            }
        }),queryInterface.addColumn('sections','secCampusId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'campuses',
                key:'id',
                as:'secCampusId',
            }

        }),queryInterface.addColumn('fees','feesStudentId' ,{

            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'students',
                key:'id',
                as:'feesStudentId',
            }

        }),queryInterface.addColumn('announcements','annouCampusId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'campuses',
                key:'id',
                as:'annouCampusId',
            }

        }),queryInterface.addColumn('exams','exSectionId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'sections',
                key:'id',
                as:'exSectionId',
            }

        }),queryInterface.addColumn('exams','exCourseId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'courses',
                key:'id',
                as:'exCourseId',
            }

        }),queryInterface.addColumn('results','resStudentId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'students',
                key:'id',
                as:'resStudentId',
            }

        }),queryInterface.addColumn('results','resCourseId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'courses',
                key:'id',
                as:'resCourseId',
            }

        }),queryInterface.addColumn('course2sections','courseToSecSectionId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'sections',
                key:'id',
                as:'courseToSecSectionId',
            }

        }),queryInterface.addColumn('course2sections','courseToSecCourseId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'courses',
                key:'id',
                as:'courseToSecCourseId',
            }

        }),queryInterface.addColumn('staff2campuses','staffToCamCampusId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'campuses',
                key:'id',
                as:'staffToCamCampusId',
            }

        }),queryInterface.addColumn('staff2campuses','staffToCamStaffId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'staffs',
                key:'id',
                as:'staffToCamStaffId',
            }

        }),queryInterface.addColumn('teacher2campuses','teachToCamCampusId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'campuses',
                key:'id',
                as:'teachToCamCampusId',
            }

        }),queryInterface.addColumn('teacher2campuses','teachToCamTeacherId' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'teachers',
                key:'id',
                as:'teachToCamTeacherId',
            }

        }),queryInterface.addColumn('teacher2courses','teacher2Id' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'teachers',
                key:'id',
                as:'teacher2id',
            }

        }),queryInterface.addColumn('teacher2courses','course2Id' ,{
            type: Sequelize.INTEGER,
            onDelete:'CASCADE',
            references:{
                model:'courses',
                key:'id',
                as:'course2Id',
            }

        })]);

    }

};