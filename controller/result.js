/**
 * Created by Idrees on 4/19/2017.
 */
var db=require("../models/index")
var announcement = require("../models/announcement")(db.sequelize,db.Sequelize);;
var campus=require("../models/campus")(db.sequelize,db.Sequelize);
var helpers=require("../helpers")
var constants=require("../contants")

exports.getResult=function (req,res) {
    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff"||req.UserDetails.role=="Student") {
        db.result.findAll({include: [db.course, db.student],
            where: {
                resStudentId: req.params.studentId,
                type: req.params.type,
                year: req.params.year,
                resCourseId:req.params.courseId,
            }
        })
            .then(function (result) {
                if (result.length>0)
                    helpers.sendResponse(res, constants.MESSAGES.FIND.SUCCESS, result, constants.HTTP.CODES.SUCCESS)
                else
                    helpers.sendResponse(res, constants.MESSAGES.FIND.NOTFOUND, [], constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
            helpers.sendResponse(res, err, null, constants.HTTP.CODES.SERVER_ERROR)

        })

    }
    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}
exports.getResults=function (req,res) {
    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff"||req.UserDetails.role=="Student") {
        db.result.findAll({include: [db.course, db.student],
            where: {
                resStudentId: req.params.studentId,
                type: req.params.type,
                year: req.params.year,
            }
        })
            .then(function (result) {
                if (result.length>0)
                    helpers.sendResponse(res, constants.MESSAGES.FIND.SUCCESS, result, constants.HTTP.CODES.SUCCESS)
                else
                    helpers.sendResponse(res, constants.MESSAGES.FIND.NOTFOUND, [], constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
            helpers.sendResponse(res, err, null, constants.HTTP.CODES.SERVER_ERROR)

        })

    }
    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}
exports.deleteResult=function (req,res) {
    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff") {
        db.result.destroy({where: {
                resStudentId: req.params.studentId,
                type: req.params.type,
                year: req.params.year,
                resCourseId:req.params.courseId,
            }
        })
            .then(function (result) {
                if (result)
                    helpers.sendResponse(res, constants.MESSAGES.DELETE.SUCCESS, result, constants.HTTP.CODES.SUCCESS)
                else
                    helpers.sendResponse(res, constants.MESSAGES.FIND.NOTFOUND, [], constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
            helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORDELETE, null, constants.HTTP.CODES.SERVER_ERROR)

        })

    }
    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}
exports.createResult=function (req,res,next) {
    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff"){
              console.log('osama',req.body)
                    db.result.build(req.body).save().then(function (result) {
                        if(result)
                        helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,result,constants.HTTP.CODES.SUCCESS)
                        else
                            helpers.sendResponse(res,constants.MESSAGES.CREATE.FAILURE,result,constants.HTTP.CODES.NOTFOUND)

                    }).catch(function (err) {
                        helpers.sendResponse(res,constants.MESSAGES.CREATE.FAILURE,null,constants.HTTP.CODES.NOTFOUND)

                    })





    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }



}

exports.updateResult=function (req,res,next) {
    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff"){
      console.log(req.body)
                    db.result.update({
                        marks:req.body.marks,
                        grade:req.body.grade,
                        resCourseId:req.body.courseId,
                        resStudentId:req.body.studentId,
                        type:req.body.type,
                        year:req.body.year

                    },{where:{resCourseId:req.params.courseId,resStudentId:req.params.studentId,type:req.params.type,year:req.params.year}}
                    ).then(function (result) {
                        if (result!=[ 0 ])
                            helpers.sendResponse(res, constants.MESSAGES.UPDATE.SUCCESS, result, constants.HTTP.CODES.UPDATE)
                        else
                            helpers.sendResponse(res, constants.MESSAGES.UPDATE.FAILURE, [], constants.HTTP.CODES.NOTFOUND)

                    })

            .catch(function (err) {

            helpers.sendResponse(res,constants.MESSAGES.UPDATE.FAILURE,null,constants.HTTP.CODES.SERVER_ERROR)
        })



    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}




