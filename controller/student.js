/**
 * Created by Idrees on 4/18/2017.
 */
var db=require("../models/index")
var helpers=require("../helpers")
var constants=require("../contants")




exports.createStudent = function (req, res, next) {

    if(req.UserDetails.role=="Admin"||req.UserDetails.role == "Staff"){
        db.section.find({where:{class:req.params.classes,name:req.params.sectionName }})
            .then(function (section) {
                console.log('osaama',section)
                if(section){
                    var postBody=helpers.request.parseBody(req.body)
                    postBody.dateOfJoin=new Date()
                    postBody.image=""
                    postBody.studSectionId=section.id;
                    db.student.build(postBody).save()
                        .then(function (student) {
                            console.log('psdasda',student)
                                helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,student,constants.HTTP.CODES.SUCCESS)
                          }).catch(function (err) {

                        helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

                    })

                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.GENERAL.FIELDS_REQUIRED,null,constants.HTTP.CODES.BAD_REQUEST)



            }).catch(function (err) {
            helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
        })


    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}
exports.getStudents = function (req, res, next) {
console.log('sdsadsadsadasda')
    if(true){
        db.student.findAll()
            .then(function (student) {

                if(student.length>0){
                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,student,constants.HTTP.CODES.SUCCESS)
                }

                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,student,constants.HTTP.CODES.NOTFOUND)


            }).catch(function (err) {
            helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
        })


    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}
exports.getStudent = function (req, res, next) {
  console.log('kashan', req.params.id)
    if(true){
        db.student.findAll({include: [{ model :db.section,include:[db.campus,db.course]}],
            where:{id:req.params.id}})
            .then(function (student) {

                     if(student&&student.length!=0){
                         helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,student,constants.HTTP.CODES.SUCCESS)
                     }

                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,student,constants.HTTP.CODES.NOTFOUND)


            }).catch(function (err) {
            helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
        })


    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}




exports.AdminOrStaffUpdateStudent= function (req, res, next) {

    if (req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff") {
                  console.log('osama',req.body)
                    db.section.findOne({
                        where:{name:req.body.sectionName,class:req.body.class,secCampusId:req.body.campusId}})
                        .then(function (section) {
                                 if(section) {

                                     db.student.update({
                                         firstName: req.body.firstName,
                                         lastName: req.body.lastName,
                                         age: req.body.age,
                                         contact: req.body.contact,
                                         email: req.body.email,
                                         address: req.body.address,
                                         studSectionId: section.id,

                                     }, {where: {id: req.body.id}}).then(function (updated) {
                                         if (updated)
                                             helpers.sendResponse(res, constants.MESSAGES.UPDATE.SUCCESS, updated, constants.HTTP.CODES.UPDATE)
                                         else
                                             helpers.sendResponse(res, constants.MESSAGES.FIND.NOTFOUND+' student', updated, constants.HTTP.CODES.NOTFOUND)

                                     }).catch(function (err) {
                                         helpers.sendResponse(res, err, null, constants.HTTP.CODES.SERVER_ERROR)

                                     })
                                 }
                                 else{
                                     helpers.sendResponse(res, constants.MESSAGES.FIND.NOTFOUND+' section',null, constants.HTTP.CODES.NOTFOUND)

                                 }
                            }).catch(function (err) {
                        helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
                    })





    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}
exports.deleteStudent= function (req, res, next) {

    if (req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff") {
        db.student.destroy({where: {id:req.params.id}}).then(function (result) {


            if (result)
                helpers.sendResponse(res, constants.MESSAGES.DELETE.SUCCESS, result,constants.HTTP.CODES.SUCCESS)
            else
                helpers.sendResponse(res, constants.MESSAGES.DELETE.NOTFOUND, result,constants.HTTP.CODES.SUCCESS)


        })
    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}