/**
 * Created by Idrees on 4/19/2017.
 */
var db=require("../models/index")
var helpers=require("../helpers")
var constants=require("../contants")

exports.addStaff2Campus=function (req,res) {
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){
        db.staff2campus.find({where:{ campusId:req.body.campId,
            staffId:req.body.staffId}}).then(function (data2) {
           if(data2){
               helpers.sendResponse(res,constants.MESSAGES.CREATE.ALREADY,null,constants.HTTP.CODES.SERVER_ERROR)
           }
           else{
               db.staff2campus.build({
                   campusId:req.body.campId,
                   staffId:req.body.staffId

               }).save().then(function (data) {
                   helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,data,constants.HTTP.CODES.SUCCESS)
               }).catch(function (err) {
                   helpers.sendResponse(res,constants.MESSAGES.CREATE.FAILURE,null,constants.HTTP.CODES.NOTFOUND)
               })
           }

        })



    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)

}

exports.deleteStaff2Campus=function (req,res) {
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){
                    db.staff2campus.destroy(
                        {
                            where:{
                                campusId:req.params.campusId,
                                staffId:req.params.staffId
                            }}).then(function (data) {
                                if(data)
                        helpers.sendResponse(res,constants.MESSAGES.DELETE.SUCCESS,data,constants.HTTP.CODES.SUCCESS)
                        else
                            helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,data,constants.HTTP.CODES.NOTFOUND)
                    }).catch(function (err) {
                        helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
                    })

    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)


}