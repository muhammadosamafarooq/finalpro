/**
 * Created by Idrees on 4/25/2017.
 */
/**
 * Created by Idrees on 4/19/2017.
 */
var db=require("../models/index")
var helpers=require("../helpers")
var constants=require("../contants")

exports.addTeacher2Course=function (req,res) {
    console.log(req.body)
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){
        db.course.find({where:{name:req.body.courseName,class:req.body.class}})
            .then(function (course)
            {

                if(course){
                    db.teacher2course.find({where:{  courseId:course.id,
                        teacherId:req.body.teacherId}}).then(function (data2) {

                        if(data2)
                            helpers.sendResponse(res,constants.MESSAGES.CREATE.ALREADY,null,constants.HTTP.CODES.SERVER_ERROR)
                        else{
                            db.teacher2course.build({
                                courseId:course.id,
                                teacherId:req.body.teacherId

                            }).save().then(function (data) {

                                if(data)
                                    helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,data,constants.HTTP.CODES.SUCCESS)
                                else
                                    helpers.sendResponse(res,constants.MESSAGES.CREATE.FAILURE,data,constants.HTTP.CODES.FAILURE)
                            }).catch(function (err) {
                                helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
                            })
                        }
                    })

                }
                else{

                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,null,constants.HTTP.CODES.BAD_REQUEST)}
            })
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)

}

exports.deleteTeacher2Course=function (req,res) {
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){
        db.course.find({where:{name:req.params.courseName,class:req.params.class}})
            .then(function (course)
            {
                if(course){
                    db.teacher2course.destroy(
                        {
                            where:{
                                courseId:course.id,
                                teacherId:req.params.teacherId
                            }}).then(function (data) {
                                if(data)
                        helpers.sendResponse(res,constants.MESSAGES.DELETE.SUCCESS,data,constants.HTTP.CODES.SUCCESS)
                        else
                            helpers.sendResponse(res,constants.MESSAGES.DELETE.FAILURE,data,constants.HTTP.CODES.NOTFOUND)
                    }).catch(function (err) {
                        helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
                    })
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,null,constants.HTTP.CODES.BAD_REQUEST)
            })
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)


}