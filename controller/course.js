/**
 * Created by Idrees on 4/19/2017.
 */
/**
 * Created by Idrees on 4/19/2017.
 */
var db=require("../models/index")
var course= require("../models/course")(db.sequelize,db.Sequelize);
var helpers=require("../helpers")
var constants=require("../contants")

exports.getCourse=function (req,res,next) {

        db.course.findAll()
            .then(function (course) {
                if (course) {
                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS, course,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND, course,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORCREATE, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )


}


exports.getCourseByNameandClass=function (req,res,next) {
    if(req.params.courseName && req.params.class) {
        db.course.findAll({include:[db.teacher,db.section],
            where:{name: req.params.courseName,class:req.params.class}})
            .then(function (course) {
                if (course&&course.length!=0) {
                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS, course,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND, course,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORCREATE, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.GENERAL,null,constants.HTTP.CODES.BAD_REQUEST)

}

exports.deleteCourse=function (req,res,next) {
    console.log(req.UserDetails.role)
    if (req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff") {
        console.log('2')
        if (req.params.courseName && req.params.class) {
            db.course.destroy({where: {name: req.params.courseName, class: req.params.class}})
                .then(function (course) {
                    if (course) {

                        helpers.sendResponse(res, constants.MESSAGES.DELETE.SUCCESS, course, constants.HTTP.CODES.SUCCESS)
                    }
                    else
                        helpers.sendResponse(res, constants.MESSAGES.FIND.NOTFOUND, course, constants.HTTP.CODES.ERROR.ERRORDELETE)

                }).catch(function (err) {
                console.log(err)
                    helpers.sendResponse(res, constants.MESSAGES.ERROR.ERRORDELETE, null, constants.HTTP.CODES.SERVER_ERROR)
                }
            )
        }
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)

}


exports.updateCourse=function (req,res) {

    if(req.UserDetails.role=="Admin" ||req.UserDetails.role=="Staff")
    {

   console.log(req.body)
        db.course.update({
            name:req.body.name,
            syllabus:req.body.syllabus,
            crTeacherId:req.body.teacherId
        },{where:{name:req.params.courseName,class:req.params.class}}).then(function (course) {
            if(course) {
                console.log('dsssssssssa')
                helpers.sendResponse(res, constants.MESSAGES.UPDATE.SUCCESS, course, constants.HTTP.CODES.UPDATE)
            }
            else
                helpers.sendResponse(res,constants.MESSAGES.UPDATE.NOTFOUND,null,constants.HTTP.CODES.SUCCESS)

        }).catch(function (err) {
            helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORUPDATE,null,constants.HTTP.CODES.SERVER_ERROR)
        })
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)

}

exports.createCourse=function (req,res) {
    if(req.UserDetails.role=="Admin" ||req.UserDetails.role=="Staff"){
         console.log('sdasdsa')
        db.course.find({where:{class:req.body.class,name:req.body.name}}).then(function (course) {
          if(course){
              helpers.sendResponse(res,constants.MESSAGES.CREATE.ALREADY,null,constants.HTTP.CODES.NOTFOUND)
          }
          else{
              db.course.build(helpers.request.parseBody(req.body))
                  .save().then(function (campus) {
                  helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,campus,constants.HTTP.CODES.SUCCESS)
              }).catch(function (err) {
                  helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

              })
          }

        })

    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
}
