/**
 * Created by Idrees on 4/19/2017.
 */
var db=require("../models/index")
var campus= require("../models/campus")(db.sequelize,db.Sequelize);;
var helpers=require("../helpers")
var constants=require("../contants")

exports.getCamp=function (req,res,next) {


        db.campus.findAll()
            .then(function (campus) {

                if (campus.length>0) {

                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS, campus,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND, campus,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORFIND, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )


}
exports.getCampusByName=function (req,res,next) {

    if(req.params.name) {

        db.campus.findAll({include:[db.admin],
            where:{name:req.params.name}})
            .then(function (campus) {

                if (campus.length>0) {

                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS, campus,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND, campus,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORFIND, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.GENERAL,null,constants.HTTP.CODES.BAD_REQUEST)

}

exports.createCampus=function (req,res) {
    if(req.UserDetails.role=="Admin" ||req.UserDetails.role=="Staff"){
      console.log(req.body)
        req.body.createdAt="";
        req.body.updatedAt="";
        req.body.image="";
        db.campus.build(helpers.request.parseBody(req.body))
            .save().then(function (campus) {
            helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,campus,constants.HTTP.CODES.SUCCESS)
        }).catch(function (err) {
            helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORCREATE,null,constants.HTTP.CODES.SERVER_ERROR)

        })
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)


}
exports.deleteCampus=function (req,res,next) {

        db.campus.destroy({where:{id:req.params.name}})
            .then(function (campus) {
                if (campus) {
                    helpers.sendResponse(res,constants.MESSAGES.DELETE.SUCCESS, campus,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.DELETE.NOTFOUND, campus,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORDELETE, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )


}

exports.updateCampus=function (req,res) {

    if (req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff") {
  console.log('sdaaaaaaaaaa',req.params.id)


        db.campus.update({
            name: req.body.name,
            location: req.body.location,
            address: req.body.address,
            adminId: req.body.adminId
        }, {where: {id: req.params.id}}).then(function (campus) {
            console.log(campus)
            if (campus==1) {console.log('1')
                helpers.sendResponse(res, constants.MESSAGES.UPDATE.SUCCESS, campus, constants.HTTP.CODES.UPDATE)

            }
            else {
                console.log('2')
                helpers.sendResponse(res, constants.MESSAGES.UPDATE.FAILURE, null, constants.HTTP.CODES.NOTFOUND)
            }

        }).catch(function (err) {
            console.log('3')
            helpers.sendResponse(res, constants.MESSAGES.UPDATE.FAILUREn, null, constants.HTTP.CODES.SERVER_ERROR)
        })
    }
    else
        helpers.sendResponse(res, constants.MESSAGES.FORBIDDEN, null, constants.HTTP.CODES.UNAUTHORIZED)

}