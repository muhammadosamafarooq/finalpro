/**
 * Created by Idrees on 4/19/2017.
 */
var db=require("../models/index")
var helpers=require("../helpers")
var constants=require("../contants")

exports.addTeacher2Campus=function (req,res) {

    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){
        console.log('sdsd',req.body)
                 db.teacher2campus.find({where:{ campusId:req.body.campusId,
                     teacherId:req.body.teacherId}}).then(function (data2) {
                     if(data2) {
                         helpers.sendResponse(res,constants.MESSAGES.CREATE.ALREADY,null,constants.HTTP.CODES.SERVER_ERROR)
                     }
                     else{
                         db.teacher2campus.build({
                             campusId:req.body.campusId,
                             teacherId:req.body.teacherId

                         }).save().then(function (data) {
                             if(data) {

                                 helpers.sendResponse(res, constants.MESSAGES.CREATE.SUCCESS, data, constants.HTTP.CODES.SUCCESS)
                             }
                         }).catch(function (err) {
                             helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORCREATE,null,constants.HTTP.CODES.SERVER_ERROR)
                         })
                     }

                 })




    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)


}

exports.deleteTeacher2Campus=function (req,res) {
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){
        db.teacher2campus.destroy(
                        {
                            where:{
                        campusId:req.params.campusId,
                        teacherId:req.params.teacherId
                    }}).then(function (data) {
                           if(data)
                        helpers.sendResponse(res,constants.MESSAGES.DELETE.SUCCESS,data,constants.HTTP.CODES.SUCCESS)
                      else
                               helpers.sendResponse(res,constants.MESSAGES.CREATE.NOTFOUND,data,constants.HTTP.CODES.NOTFOUND)
                    }).catch(function (err) {
            console.log(err)
                        helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORDELETE,null,constants.HTTP.CODES.SERVER_ERROR)
                    })


    }
    else {
        console.log('dssad')
        helpers.sendResponse(res, constants.MESSAGES.FORBIDDEN, null, constants.HTTP.CODES.UNAUTHORIZED)
    }


}
