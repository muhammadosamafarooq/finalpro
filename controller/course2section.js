
/**
 * Created by Idrees on 4/19/2017.
 */
var db=require("../models/index")
var helpers=require("../helpers")
var constants=require("../contants")

exports.addCourse2Section=function (req,res) {
   console.log(req.body)
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){
        db.course.find({where:{name:req.body.courseName,class:req.body.class}})
            .then(function (course){
                if(course){

                    db.section.find({where:{name:req.body.sectionName,class:req.body.secClass}})
                        .then(function (section){
                    if(section){
                        db.course2section.find({where:{ courseId:course.id,
                            sectionId:section.id}}).then(function (data) {
                           if(data){
                              helpers.sendResponse(res,constants.MESSAGES.CREATE.ALREADY,null,constants.HTTP.CODES.SERVER_ERROR)

                           }
                           else{
                               db.course2section.build({
                                   courseId:course.id,
                                   sectionId:section.id

                               }).save().then(function (data) {
                                   helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,data,constants.HTTP.CODES.SUCCESS)
                               }).catch(function (err) {
                                   helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
                               })

                           }
                        })
                    }
                    else
                        helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND+'  section',null,constants.HTTP.CODES.NOTFOUND)
                })
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND+'  course',null,constants.HTTP.CODES.NOTFOUND)

                }).catch(function (err) {
            helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
        })

    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)

}

exports.deleteCourse2Section=function (req,res) {
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){
        db.course.find({name:req.params.courseName,class:req.params.class})
            .then(function (course){
                if(course){
                    db.section.find({where:{name:req.params.sectionName,class:req.params.secClass}})
                        .then(function (section){
                            if(section){

                                db.course2section.destroy({where:{
                                    courseId:course.id,
                                    sectionId:section.id

                                }}).then(function (data) {
                                    if(data)
                                    helpers.sendResponse(res,constants.MESSAGES.DELETE.SUCCESS,data,constants.HTTP.CODES.SUCCESS)
                                    else
                                        helpers.sendResponse(res,constants.MESSAGES.DELETE.FAILURE,data,constants.HTTP.CODES.NOTFOUND)
                                }).catch(function (err) {
                                    helpers.sendResponse(res,constants.MESSAGES.DELETE.FAILURE,null,constants.HTTP.CODES.NOTFOUND)
                                })
                            }
                            else
                                helpers.sendResponse(res,constants.MESSAGES.FIND.FAILURE+' section',null,constants.HTTP.CODES.BAD_REQUEST)
                        })
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.FAILURE+' course',null,constants.HTTP.CODES.BAD_REQUEST)

            }).catch(function (err) {
            helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
        })

    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)

}
