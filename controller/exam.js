/**
 * Created by Idrees on 4/19/2017.
 */
var db=require("../models/index")
var announcement = require("../models/announcement")(db.sequelize,db.Sequelize);;
var campus=require("../models/campus")(db.sequelize,db.Sequelize);
var helpers=require("../helpers")
var constants=require("../contants")

exports.getExam=function (req,res) {
    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff") {
        db.exam.findAll({include:[{model:db.section,include:[db.campus]},db.course],where:{exSectionId: req.params.sectionId
            ,exCourseId:req.params.courseId}})
            .then(function (exam) {
                if(exam.length>0)
                helpers.sendResponse(res, constants.MESSAGES.FIND.SUCCESS, exam, constants.HTTP.CODES.SUCCESS)
                else
                    helpers.sendResponse(res, constants.MESSAGES.FIND.FAILURE, [], constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
            helpers.sendResponse(res, err, null, constants.HTTP.CODES.SERVER_ERROR)
        })
    }
    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}

exports.getExamBySection=function (req,res) {
    if( req.UserDetails.role=="Student"||req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff") {
       console.log("phansa nahi",req.params.sectionId)
        db.exam.findAll({include:[{model:db.section,include:[db.campus]},db.course],where:{exSectionId: req.params.sectionId}})
            .then(function (exam) {
                    if(exam.length>0)
                helpers.sendResponse(res, constants.MESSAGES.FIND.SUCCESS, exam, constants.HTTP.CODES.SUCCESS)
                    else
                        helpers.sendResponse(res, constants.MESSAGES.FIND.FAILURE, [], constants.HTTP.CODES.SUCCESS)

            }).catch(function (err) {
            helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORFIND, null, constants.HTTP.CODES.SERVER_ERROR)
        })
    }
    else
    {
        //console.log("phansa")
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}


exports.deleteExam=function (req,res) {
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff") {
        console.log("phansa nahi")
        db.exam.destroy({where:{exSectionId: req.params.sectionId,exCourseId:req.params.courseId}})
            .then(function (exam) {
                if(exam)
                    helpers.sendResponse(res, constants.MESSAGES.DELETE.SUCCESS, exam, constants.HTTP.CODES.SUCCESS)
                else
                    helpers.sendResponse(res, constants.MESSAGES.DELETE.FAILURE, [], constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
            helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORDELETE, null, constants.HTTP.CODES.SERVER_ERROR)
        })
    }
    else
    {
        //console.log("phansa")
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}


exports.createExam=function (req,res,next) {
    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff"){

                               console.log('wahsadsad')
                                            db.exam.build({
                                        date:req.body.date,
                                        exSectionId:req.body.sectionId,
                                        exCourseId:req.body.courseId
                                    }).save().then(function (exam) {
                                        if(exam)
                                       helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,exam,constants.HTTP.CODES.SUCCESS)
                                       else
                                            helpers.sendResponse(res,constants.MESSAGES.CREATE.FAILURE,null,constants.HTTP.CODES.SERVER_ERROR)

                                            }).catch(function (err) {

                                        helpers.sendResponse(res,constants.MESSAGES.CREATE.FAILURE,null,constants.HTTP.CODES.SERVER_ERROR)
                                    })
                                }


    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}

exports.updateExam=function (req,res,next) {
    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff"){
        console.log('sdsadsadsad',req.body)

                                    db.exam.update({date:req.body.date,
                                        exSectionId:req.body.exSectionId,
                                        exCourseId:req.body.exCourseId
                                        },{where:{exSectionId:req.params.sectionId,exCourseId:req.params.courseId}}
                                        ).then(function (data) {

                                        if(data!=[ 0 ]){

                                            helpers.sendResponse(res,constants.MESSAGES.UPDATE.SUCCESS,null,constants.HTTP.CODES.UPDATE)
                                        }
                                        else
                                            helpers.sendResponse(res,constants.MESSAGES.UPDATE.FAILURE,null,constants.HTTP.CODES.NOTFOUND)
                                    }).catch(function (err) {
                                        console.log(err)
                                        helpers.sendResponse(res,constants.MESSAGES.UPDATE.FAILURE,null,constants.HTTP.CODES.NOTFOUND)
                                    })


    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}





