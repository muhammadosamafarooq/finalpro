/**
 * Created by Idrees on 4/18/2017.
 */
var db=require("../models/index")
var admin = require("../models/admin")(db.sequelize,db.Sequelize);
var staff = require("../models/staff")(db.sequelize,db.Sequelize);
var teacher = require("../models/teacher")(db.sequelize,db.Sequelize);
var student = require("../models/student")(db.sequelize,db.Sequelize);
var jwt = require("jsonwebtoken")
var helpers=require("../helpers")
var constants=require("../contants")


exports._teacher = function (req, res, next) {

    db.teacher.find({include:[db.course,db.campus],where:{id: req.body.id}})
        .then(function (teacher) {
            if(teacher){
            if (teacher.password==req.body.password) {

                let t_token = jwt.sign({
                    role: "Teacher",
                    id: teacher.id
                }, "secretKey", {expiresIn: "2 days"})
                req.UserDetails = t_token;
                teacher.token=t_token
                teacher.save()

                helpers.sendResponse(res,constants.MESSAGES.LOGIN.SUCCESS,teacher,constants.HTTP.CODES.SUCCESS)
            }
            else {
                helpers.sendResponse(res,constants.MESSAGES.LOGIN.AUTH_FAILED,null,constants.HTTP.CODES.UNAUTHORIZED)
            }} else {
                helpers.sendResponse(res,constants.MESSAGES.LOGIN.AUTH_FAILED,null,constants.HTTP.CODES.UNAUTHORIZED)
            }


        }).catch(function (err) {
        helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

        })


}

exports._staff = function (req, res, next) {

    db.staff.find({include:[db.campus],where:{id:req.body.id}})
        .then(function (staff) {
            if(staff){
            if (staff.password==req.body.password) {
                let t_token = jwt.sign({
                    role: "Staff",
                    id: staff.id
                }, "secretKey", {expiresIn: "2 days"})
                req.UserDetails = staff.id;
                staff.token=t_token
                staff.save()
                helpers.sendResponse(res,constants.MESSAGES.LOGIN.SUCCESS,staff,constants.HTTP.CODES.SUCCESS)


            }
            else {
                console.log("staff",staff.firstName+staff.lastName)
                helpers.sendResponse(res,constants.MESSAGES.LOGIN.AUTH_FAILED,null,constants.HTTP.CODES.UNAUTHORIZED)
            }
            } else {
                helpers.sendResponse(res,constants.MESSAGES.LOGIN.AUTH_FAILED,null,constants.HTTP.CODES.UNAUTHORIZED)
            }


        }).catch(function (err) {
        helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

        })





}

exports._student = function (req, res, next) {
  console.log("----------------------------------------",req.body)

    db.student.find({include: [{model :db.section,include:[db.campus,db.course]}],where:{id:req.body.id}}
    ).then(function (student) {

        if(student){
               if (student.password==req.body.password) {
                let t_token = jwt.sign({
                    role: "Student",
                    id: student.id
                }, "secretKey", {expiresIn: "2 days"})
             //req.UserDetails.id = student.id;
                student.token=t_token
                student.save()

                helpers.sendResponse(res,constants.MESSAGES.LOGIN.SUCCESS,student,constants.HTTP.CODES.SUCCESS)
            }
            else {
                   console.log("1")
                helpers.sendResponse(res,constants.MESSAGES.LOGIN.AUTH_FAILED,null,constants.HTTP.CODES.UNAUTHORIZED)
            }
        } else {
            console.log("2")
            helpers.sendResponse(res,constants.MESSAGES.LOGIN.AUTH_FAILED,null,constants.HTTP.CODES.UNAUTHORIZED)
        }


        }).catch(function (err) {
        helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

        })


}

exports._admin = function (req, res, next) {

    db.admin.find({include:[{model:db.campus}],where:{id:req.body.id}}
    ).then(function (admin) {
        if(admin){
            if (admin.password==req.body.password) {
                let t_token = jwt.sign({
                    role: "Admin",
                    id: admin.id
                }, "secretKey", {expiresIn: "2 days"})
                admin.token=t_token

                admin.save()
                helpers.sendResponse(res,constants.MESSAGES.LOGIN.SUCCESS,admin,constants.HTTP.CODES.SUCCESS)
            }
            else {
                helpers.sendResponse(res,constants.MESSAGES.LOGIN.AUTH_FAILED,null,constants.HTTP.CODES.UNAUTHORIZED)
            }} else {
            helpers.sendResponse(res,constants.MESSAGES.LOGIN.AUTH_FAILED,null,constants.HTTP.CODES.UNAUTHORIZED)
        }


        }).catch(function (err) {
            console.log(err)
        helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
        })


}