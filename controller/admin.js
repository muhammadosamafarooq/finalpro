/**
 * Created by Idrees on 4/18/2017.
 */
var db=require("../models/index")
var helpers=require("../helpers")
var constants=require("../contants")





exports.createAdmin = function (req, res, next) {

 console.log(req.body)
    if(req.UserDetails.role == "Admin"&&req.UserDetails.id==1) {

        var postBody=helpers.request.parseBody(req.body)
        postBody.image=""
        postBody.dateOfJoin=new Date();
        db.admin.build(postBody).save()
            .then(function (admin) {
                helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,admin,constants.HTTP.CODES.SUCCESS)
            }).catch(function (err) {
            helpers.sendResponse(res,err,null,constants.HTTP.CODES.ERROR.ERRORCREATE)

        })

    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}
exports.getAdmins=function (req,res,next) {
    if(req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff") {
        db.admin.findAll()
            .then(function (result) {
                if (result&&result.length!=0) {
                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,result,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,result,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORFIND, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.GENERAL,null,constants.HTTP.CODES.BAD_REQUEST)

}
exports.getAdmin=function (req,res,next) {
    if(req.UserDetails.role == "Admin"&&req.UserDetails.id==1) {
        db.admin.findAll({where:{id: req.params.id}})
            .then(function (result) {
                if (result&&result.length!=0) {
                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,result,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,result,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORFIND, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.GENERAL,null,constants.HTTP.CODES.BAD_REQUEST)

}


exports.AdminupdateAdmin= function (req, res, next) {


    if(req.UserDetails.role == "Admin"&&req.UserDetails.id==1) {
        console.log('sdsadsadasdsad')
        db.admin.update({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            age: req.body.age,
            contact: req.body.contact,
            email: req.body.email,
            address: req.body.address,
            salary: req.body.salary,

        }, {where: {id: req.params.id}}).then(function (updated) {
            console.log('andar')
            if (updated == 1)
                helpers.sendResponse(res, constants.MESSAGES.UPDATE.SUCCESS, updated, constants.HTTP.CODES.UPDATE)
            else
                helpers.sendResponse(res, constants.MESSAGES.UPDATE.FAILURE, updated, constants.HTTP.CODES.NOTFOUND)

        }).catch(function (err) {
            console.log('sdsadsdasdad',constants.MESSAGES.UPDATE.FAILURE)
               helpers.sendResponse(res,constants.MESSAGES.UPDATE.FAILURE, null, constants.HTTP.CODES.SERVER_ERROR)

        })

    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}


exports.deleteAdmin= function (req, res, next) {

    if (req.UserDetails.role == "Admin"&&req.UserDetails.id == 1) {

        db.admin.destroy({where: {id:req.params.id}}).then(function (result) {


            if (result)
                helpers.sendResponse(res, constants.MESSAGES.DELETE.SUCCESS, result,constants.HTTP.CODES.SUCCESS)
            else
                helpers.sendResponse(res, constants.MESSAGES.DELETE.NOTFOUND,null,constants.HTTP.CODES.NOTFOUND)


        })
    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}