/**
 * Created by Idrees on 4/18/2017.
 */
var db=require("../models/index")
var fees = require("../models/fee")(db.sequelize,db.Sequelize);
var helpers=require("../helpers")
var constants=require("../contants")


exports.createFee = function (req, res, next) {

    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff") {
        req.body.dateOfPayment=new Date();
        db.fee.build(helpers.request.parseBody(req.body))
            .save()
            .then(function (fee) {
                helpers.sendResponse(res, constants.MESSAGES.CREATE.SUCCESS, fee,constants.HTTP.CODES.SUCCESS)
            }).catch(function (err) {
            helpers.sendResponse(res, constants.MESSAGES.CREATE.FAILURE, err,constants.HTTP.CODES.NOTFOUND)

        })

    }
    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,constants.HTTP.CODES.SERVER_ERROR)
    }

}

exports.getFee = function (req, res, next) {
  console.log(req.body)
    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff") {
       db.fee.findAll({include:[db.student],where:{feesStudentId:req.params.studentId,type:req.params.type,detail:req.params.detail}})
           .then(function (fee) {
               if(fee.length>0){
                   helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,fee,constants.HTTP.CODES.SUCCESS)
               }
               else
                   helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,null,constants.HTTP.CODES.NOTFOUND)



           }).catch(function (err) {
               helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

       })

    }
    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}
exports.deleteFee = function (req, res, next) {

    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff") {
        db.fee.destroy({where:{feesStudentId:req.params.studentId,type:req.params.type,detail:req.params.detail}})
            .then(function (fee) {
                if(fee){
                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,fee,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,null,constants.HTTP.CODES.SUCCESS)



            }).catch(function (err) {
            helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

        })

    }
    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}

exports.updateFees=function (req,res) {

    if (req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff") {

           console.log('osama',req.body)
        console.log('osama',req.params.studentId)
        console.log('osama',req.params.type)
        console.log('osama',req.params.detail)
        db.fee.update({
            type: req.body.type,
            detail: req.body.detail,
            dueDate: req.body.dueDate,
            feesStudentId: req.body.studentId,
        },{where:{feesStudentId:req.params.studentId,type:req.params.type,detail:req.params.detail}}).then(function (detail) {
            console.log('sasdsad')
            if (detail==1) {
                helpers.sendResponse(res, constants.MESSAGES.UPDATE.SUCCESS,detail, constants.HTTP.CODES.UPDATE)

            }
            else
                helpers.sendResponse(res, constants.MESSAGES.UPDATE.FAILURE , null, constants.HTTP.CODES.NOTFOUND)

        }).catch(function (err) {
            helpers.sendResponse(res, constants.MESSAGES.ERROR.ERRORUPDATE, null, constants.HTTP.CODES.SERVER_ERROR)
        })
    }
    else
        helpers.sendResponse(res, constants.MESSAGES.FORBIDDEN, null, constants.HTTP.CODES.UNAUTHORIZED)

}


exports.toggleStatus = function (req, res, next) {

    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff") {
        db.fee.find({feesStudentId:req.params.studentId,type:req.params.type,detail:req.params.detail})
            .then(function (fee) {
                if(fee){
                    fee.status="paid"
                    fee.dateOfPayment=new Date()
                    fee.save()
                        .then(function (fee) {
                            helpers.sendResponse(res,constants.MESSAGES.UPDATE.SUCCESS.fee,constants.HTTP.CODES.UPDATE)

                        })

                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,null,constants.HTTP.CODES.SUCCESS)


            }).catch(function (err) {
            helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

        })

    }
    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}

