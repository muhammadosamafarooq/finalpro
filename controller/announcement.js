/**
 * Created by Idrees on 4/19/2017.
 */
var db=require("../models/index")
var announcement = require("../models/announcement")(db.sequelize,db.Sequelize);;
var campus=require("../models/campus")(db.sequelize,db.Sequelize);
var helpers=require("../helpers")
var constants=require("../contants")

exports.getAnnouncementByCampus=function (req,res,next) {
    if(true){

                db.announcement.findAll({inculde:[db.campus],where:{annouCampusId:req.params.id}})
                    .then(function (announcement) {
                        if(announcement.length>0)
                        helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,announcement,constants.HTTP.CODES.SUCCESS)
                        else
                            helpers.sendResponse(res, constants.MESSAGES.FIND.FAILURE, [], constants.HTTP.CODES.NOTFOUND)
                }).catch(function (err) {
                    helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORFIND,null,constants.HTTP.CODES.SERVER_ERROR)

                }).catch(function (err) {
            helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORFIND,null,constants.HTTP.CODES.SERVER_ERROR)
        })
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.GENERAL,null,constants.HTTP.CODES.BAD_REQUEST)


}

exports.deleteAnnouncement=function (req,res,next) {
    if(req.params.campusName){
        db.campus.find({where:{name:req.params.campusName} })
            .then(function (campus) {
                if(campus){
                    db.announcement.destroy({inculde:[db.campus],where:{annouCampusId:campus.id}})
                        .then(function (announcement) {
                            if(announcement.length>0)
                                helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,announcement,constants.HTTP.CODES.SUCCESS)
                            else
                                helpers.sendResponse(res, constants.MESSAGES.FIND.SUCCESS, [], constants.HTTP.CODES.SUCCESS)
                        }).catch(function (err) {
                        helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

                    })

                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.GENERAL,null,constants.HTTP.CODES.BAD_REQUEST)
            }).catch(function (err) {

            helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
        })
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.GENERAL,null,constants.HTTP.CODES.BAD_REQUEST)


}


exports.createAnnouncement=function (req,res,next) {
    if(req.UserDetails.role=="Admin" || req.UserDetails.role=="Staff") {
            console.log(req.body)
                    db.announcement.build({
                        annouCampusId: req.body.campusId,
                        title: req.body.title,
                        detail: req.body.detail

                    }).save().then(function (announcement) {
                        helpers.sendResponse(res, constants.MESSAGES.CREATE.SUCCESS, announcement,constants.HTTP.CODES.SUCCESS)

                    }).catch(function (err) {

            helpers.sendResponse(res,constants.MESSAGES.CREATE.FAILURE,null,constants.HTTP.CODES.NOTFOUND)
        })
    }
    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }
}



