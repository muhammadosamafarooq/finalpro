/**
 * Created by Idrees on 4/18/2017.
 */
var db=require("../models/index")
var staff = require("../models/staff")(db.sequelize,db.Sequelize);
var helpers=require("../helpers")
var constants=require("../contants")



exports.createStaff = function (req, res, next) {

    if(req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff") {
        var postBody=helpers.request.parseBody(req.body)
        postBody.dateOfJoin=new Date()
        postBody.image=""
        db.staff.build(postBody).save()
            .then(function (staff) {

            helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,staff,constants.HTTP.CODES.SUCCESS)
            }).catch(function (err) {
            helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

        })

    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}
exports.getStaffAll=function (req,res,next) {

    if(req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff")  {
        db.staff.findAll()
            .then(function (result) {
                if (result) {

                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,result,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,result,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res, err, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.GENERAL.FIELDS_REQUIRED,null,constants.HTTP.CODES.BAD_REQUEST)

}

exports.getStaff=function (req,res,next) {

    if(req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff")  {
        db.staff.findOne({include:[db.campus],where:{id: req.params.id}})
            .then(function (result) {
                if (result) {

                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,result,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND,result,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res, err, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.GENERAL.FIELDS_REQUIRED,null,constants.HTTP.CODES.BAD_REQUEST)

}


exports.AdminUpdateStaff= function (req, res, next) {


    if (req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff") {
          console.log("dasdadass")

            db.staff.update({
                    firstName:req.body.firstName,
                    lastName:req.body.lastName,
                    age:req.body.age,
                    contact:req.body.contact,
                    email:req.body.email,
                    address:req.body.address,
                    salary:req.body.salary,

                },{where:{id:req.params.id}}).then(function (updated) {
                if(updated)
                    helpers.sendResponse(res,constants.MESSAGES.UPDATE.SUCCESS,updated,constants.HTTP.CODES.UPDATE)
                else
                    helpers.sendResponse(res,constants.MESSAGES.DATABASE_ERR,updated,constants.HTTP.CODES.BAD_REQUEST)

            }).catch(function (err) {
                helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

            })



    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}

exports.deleteStaff= function (req, res, next) {

    if (req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff") {
        db.staff.destroy({where: {id:req.params.id}}).then(function (responce) {


            if (responce)
                helpers.sendResponse(res, constants.MESSAGES.DELETE.SUCCESS, responce,constants.HTTP.CODES.SUCCESS)
            else
                helpers.sendResponse(res, constants.MESSAGES.DELETE.NOTFOUND, responce,constants.HTTP.CODES.SUCCESS)


        }).catch(function (err) {
            console.log(err)
            helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)

        })
    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}
