/**
 * Created by Idrees on 4/18/2017.
 */
var db=require("../models/index")
var teacher = require("../models/teacher");
var campus= require("../models/campus")(db.sequelize,db.Sequelize);;
var helpers=require("../helpers")
var constants=require("../contants")



exports.createTeacher = function (req, res, next) {
   console.log(req.body)
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){

        var postBody=helpers.request.parseBody(req.body)
        postBody.dateOfJoin=new Date();
        postBody.image=""
        db.teacher.build(postBody).save()
                    .then(function (teacher) {

                     helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,teacher,constants.HTTP.CODES.SUCCESS)
                           }).catch(function (err) {

            helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORCREATE,null,constants.HTTP.CODES.SERVER_ERROR)

        })

    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORCREATE,null,constants.HTTP.CODES.UNAUTHORIZED)
    }

}




exports.getTeacherAll=function (req,res,next) {

    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff") {

        db.teacher.findAll()
            .then(function (teacher) {
                if (teacher) {

                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS, teacher,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND, teacher,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORFIND, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORFIND,null,constants.HTTP.CODES.BAD_REQUEST)

}

exports.getTeacher=function (req,res,next) {
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff") {
        db.teacher.find({include:[db.course,db.campus],where:{id: req.params.id}})
            .then(function (teacher) {
                if (teacher) {

                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS, teacher,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND, teacher,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORFIND, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )
    }
    else
        helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORFIND,null,constants.HTTP.CODES.BAD_REQUEST)

}

exports.getTeacherByName=function (req,res,next) {
            db.teacher.find({where:{firstName:req.params.firstName,lastName:req.params.lastName}})
            .then(function (teacher) {
                if (teacher) {

                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS, teacher,constants.HTTP.CODES.SUCCESS)
                }
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.NOTFOUND, teacher,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res, err, null,constants.HTTP.CODES.SERVER_ERROR)
            }
        )


}




exports.AdminOrStaffUpdateteacher= function (req, res, next) {


    if (req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff") {

            db.teacher.update({
                    firstName:req.body.firstName,
                    lastName:req.body.lastName,
                    age:req.body.age,
                    contact:req.body.contact,
                    email:req.body.email,
                    address:req.body.address,
                    salary:req.body.salary,
                    qualification:req.body.qualification
                },{where :{id:req.params.id}}).then(function (updated) {
                if(updated==1) {
                    console.log('232')
                    helpers.sendResponse(res, constants.MESSAGES.UPDATE.SUCCESS, updated, constants.HTTP.CODES.UPDATE)
                }
                else
                    helpers.sendResponse(res,constants.constants.MESSAGES.UPDATE.NOTFOUND,updated,constants.HTTP.CODES.NOTFOUND)

            }).catch(function (err) {
                helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORUPDATE,null,constants.HTTP.CODES.SERVER_ERROR)

            })



    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORUPDATE,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}

exports.deleteTeacher= function (req, res, next) {

    if (req.UserDetails.role == "Admin"||req.UserDetails.role == "Staff") {
        db.teacher.destroy({where: {id:req.params.id}}).then(function (responce) {


            if (responce) {
                helpers.sendResponse(res, constants.MESSAGES.DELETE.SUCCESS, responce, constants.HTTP.CODES.SUCCESS)
            }
            else
                helpers.sendResponse(res, constants.MESSAGES.DELETE.NOTFOUND, responce,constants.HTTP.CODES.NOTFOUND)


        }).catch(function (err) {
            console.log('re')
            helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORDELETE,null,constants.HTTP.CODES.SERVER_ERROR)

        })
    }

    else
    {
        helpers.sendResponse(res,constants.MESSAGES.ERROR.ERRORDELETE,null,constants.HTTP.CODES.UNAUTHORIZED)
    }


}


/**
 * Created by Idrees on 4/19/2017.
 */
