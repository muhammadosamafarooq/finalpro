var db=require("../models/index")
var helpers=require("../helpers")
var constants=require("../contants")

exports.createSection=function (req,res) {
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){
        db.section.find({where:{name:req.body.name,class:req.body.class}}).then(function (data2) {
           console.log('sdasdsdsadsdddddddddddd',data2)
            if(data2){
                helpers.sendResponse(res,constants.MESSAGES.CREATE.ALREADY,null,constants.HTTP.CODES.NOTFOUND)

            }
            else {
                db.section.build({
                    name:req.body.sectionName,
                    class:req.body.class,
                    secTeacherId:req.body.teacherId,
                    secCampusId:req.body.campusId
                })
                    .save().then(function (data) {
                    helpers.sendResponse(res,constants.MESSAGES.CREATE.SUCCESS,data,constants.HTTP.CODES.SUCCESS)
                }).catch(function (err) {
                    helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
                })
            }
        })


    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)


}

exports.updateSection=function (req,res) {
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){


        console.log('sdsad',req.params.campusName)

        console.log(req.body)
        db.section.update({class:req.body.class,secTeacherId:req.body.secTeacherId,secCampusId:req.body.secCampusId
                        ,name:req.body.name}
                    ,{where:{name:req.params.sectionName,class:req.params.class,secCampusId:req.params.campusName}})
                        .then(function (data) {
                         console.log('sdsad',data)
                           if(data!=[ 0 ]) {

                               helpers.sendResponse(res, constants.MESSAGES.UPDATE.SUCCESS, data, constants.HTTP.CODES.UPDATE)
                           }
                            else
                               helpers.sendResponse(res,constants.MESSAGES.UPDATE.FAILURE,null,constants.HTTP.CODES.NOTFOUND)

                    }).catch(function (err) {
                        console.log(err)
                        helpers.sendResponse(res,constants.MESSAGES.UPDATE.FAILURE,null,constants.HTTP.CODES.NOTFOUND)
                    })





    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)


}

exports.getSectionAll=function (req,res) {
    if(true){
        db.section.findAll()
            .then(function (data) {
                if(data)
                    helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,data,constants.HTTP.CODES.SUCCESS)
                else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.FAILURE,data,constants.HTTP.CODES.NOTFOUND)
            }).catch(function (err) {
            helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
        })

    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)


}

exports.getSection=function (req,res) {
    if(true){

              console.log('sdasdsa',req.params.sectionName)
               console.log(req.params.class)
        console.log(req.params.campusName)
                    db.section.findAll({include:[db.course,db.campus,db.teacher],
                        where:{name:req.params.sectionName,class:req.params.class,secCampusId:req.params.campusName}})
                .then(function (data) {
                    if(data&&data.length!=0)
                        helpers.sendResponse(res,constants.MESSAGES.FIND.SUCCESS,data,constants.HTTP.CODES.SUCCESS)
                    else
                    helpers.sendResponse(res,constants.MESSAGES.FIND.FAILURE,data,constants.HTTP.CODES.NOTFOUND)
                    }).catch(function (err) {
                        helpers.sendResponse(res,err,null,constants.HTTP.CODES.SERVER_ERROR)
                    })

    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)


}
exports.deleteSection=function (req,res) {
    if(req.UserDetails.role=="Admin"||req.UserDetails.role=="Staff"){

                    db.section.destroy({ where:{name:req.params.sectionName,class:req.params.class,secCampusId:req.params.campusName}})
                        .then(function (data) {
                            if(data)
                            helpers.sendResponse(res,constants.MESSAGES.DELETE.SUCCESS,data,constants.HTTP.CODES.SUCCESS)
                            else
                                helpers.sendResponse(res,constants.MESSAGES.FIND.FAILURE,data,constants.HTTP.CODES.NOTFOUND)
                        }).catch(function (err) {
                        helpers.sendResponse(res,constants.MESSAGES.FIND.FAILURE,null,constants.HTTP.CODES.NOTFOUND)
                    })


    }
    else
        helpers.sendResponse(res,constants.MESSAGES.FORBIDDEN,null,constants.HTTP.CODES.UNAUTHORIZED)


}