'use strict';
module.exports = function(sequelize, DataTypes) {
  var exam = sequelize.define('exam', {

    date: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here

          exam.belongsTo(models.section, {foreignKey: 'exSectionId'})
          exam.belongsTo(models.course, {foreignKey: 'exCourseId'})



      }
    }
  });
  return exam;
};