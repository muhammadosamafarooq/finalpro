'use strict';
module.exports = function(sequelize, DataTypes) {
  var campus = sequelize.define('campus', {
    name: DataTypes.STRING,
    location: DataTypes.STRING,
    address: DataTypes.STRING,
    image: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          campus.belongsToMany(models.teacher,{
              through:{
                  model: models.teacher2campus,
                  foreignKey: "campusId"

              }
          });

          campus.belongsToMany(models.staff,{
              through:{
                  model: models.staff2campus,
                  foreignKey: "campusId",


              }
          });

          campus.belongsTo(models.admin, {foreignKey: 'adminId'})
          campus.hasMany(models.section, {foreignKey:"secCampusId"})
          campus.hasMany(models.announcement, {foreignKey:"annouCampusId"})

      }
    }
  });
  return campus;
};