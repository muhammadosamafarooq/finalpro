'use strict';
module.exports = function(sequelize, DataTypes) {
    var teacher = sequelize.define('teacher', {
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        age: DataTypes.INTEGER,
        contact: DataTypes.STRING,
        address: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        gender: DataTypes.STRING,
        token: DataTypes.STRING,
        image: DataTypes.STRING,
        salary: DataTypes.INTEGER,
        dateOfJoin: DataTypes.DATE,
        qualification: DataTypes.STRING
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
                teacher.belongsToMany(models.campus,{
                    through:{
                        model: models.teacher2campus,
                        foreignKey: "teacherId"

                    }
                });
                teacher.belongsToMany(models.course,{
                    through:{
                        model: models.teacher2course,
                        foreignKey: "teacherId"


                    }
                });
                // teacher.belongsToMany(models.course, {foreignKey:"crTeacherId"})
                teacher.hasMany(models.section, {foreignKey:"secTeacherId"})
            }
        }
    });
    return teacher;
};