'use strict';
module.exports = function(sequelize, DataTypes) {
  var announcement = sequelize.define('announcement', {
    title: DataTypes.STRING,
    detail: DataTypes.STRING,
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          announcement.belongsTo(models.campus, {foreignKey: 'annouCampusId'})
      }
    }
  });
  return announcement;
};