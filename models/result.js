'use strict';
module.exports = function(sequelize, DataTypes) {
  var result = sequelize.define('result', {

    marks: DataTypes.INTEGER,
    grade: DataTypes.STRING,
    type: DataTypes.STRING,
    year: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          result.belongsTo(models.student, {foreignKey: 'resStudentId'})
          result.belongsTo(models.course, {foreignKey: 'resCourseId'})
      }
    }
  });
  return result;
};