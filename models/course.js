'use strict';
module.exports = function(sequelize, DataTypes) {
  var course = sequelize.define('course', {
    name: DataTypes.STRING,
    syllabus: DataTypes.STRING,
    class: DataTypes.INTEGER,

  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          course.belongsToMany(models.section,{
              through:{
                  model: models.course2section,
                  foreignKey: "courseId",


              }
          });
          course.belongsToMany(models.teacher,{
              through:{
                  model: models.teacher2course,
                  foreignKey: "courseId"


              }
          });
        //  course.belongsTo(models.teacher, {foreignKey: 'crTeacherId'})
          course.hasMany(models.result, {foreignKey:"resCourseId"})
          course.hasMany(models.exam, {foreignKey:"exCourseId"})

      }
    }
  });
  return course;
};