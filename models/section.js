'use strict';
module.exports = function(sequelize, DataTypes) {
  var section = sequelize.define('section', {
    name: DataTypes.STRING,
    class: DataTypes.INTEGER,

  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          section.belongsToMany(models.course,{
              through:{
                  model: models.course2section,
                  foreignKey: "sectionId"

              }
          });
          section.belongsTo(models.campus, {foreignKey: 'secCampusId'})
          section.belongsTo(models.teacher, {foreignKey: 'secTeacherId'})
          section.hasMany(models.exam, {foreignKey:"exSectionId"})
          section.hasMany(models.student, {foreignKey:"studSectionId"})
      }
    }
  });
  return section;
};