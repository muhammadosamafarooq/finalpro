'use strict';
module.exports = function(sequelize, DataTypes) {

    var admin = sequelize.define('admin', {
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        age: DataTypes.INTEGER,
        contact: DataTypes.STRING,
        address: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        gender: DataTypes.STRING,
        token: DataTypes.STRING,
        image: DataTypes.STRING,
        dateOfJoin: DataTypes.DATE,
        salary: DataTypes.INTEGER
    }, {
        classMethods: {
            associate: function(models) {
                admin.hasMany(models.campus, {foreignKey:"adminId"})

                // associations can be defined here
            }
        }
    });
    return admin;
};