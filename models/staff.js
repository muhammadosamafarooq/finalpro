'use strict';
module.exports = function(sequelize, DataTypes) {
    var staff = sequelize.define('staff', {
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        age: DataTypes.INTEGER,
        contact: DataTypes.STRING,
        address: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        gender: DataTypes.STRING,
        token: DataTypes.STRING,
        image: DataTypes.STRING,
        salary: DataTypes.INTEGER,
        dateOfJoin: DataTypes.DATE
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
                staff.belongsToMany(models.campus,{
                    through:{
                        model: models.staff2campus,
                        foreignKey: "staffId"


                    }
                });


            }
        }
    });
    return staff;
};