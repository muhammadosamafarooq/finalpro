'use strict';
module.exports = function(sequelize, DataTypes) {
  var fee = sequelize.define('fee', {

    type: DataTypes.STRING,
    detail: DataTypes.STRING,
    status: DataTypes.STRING,
    dateOfPayment: DataTypes.DATE,
    dueDate: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          fee.belongsTo(models.student, {foreignKey: 'feesStudentId'})
      }
    }
  });
  return fee;
};