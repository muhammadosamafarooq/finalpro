'use strict';
module.exports = function(sequelize, DataTypes) {
    var student = sequelize.define('student', {
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        age: DataTypes.INTEGER,
        contact: DataTypes.STRING,
        address: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        gender: DataTypes.STRING,
        token: DataTypes.STRING,
        image: DataTypes.STRING,
        dateOfJoin: DataTypes.DATE,

    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
                student.belongsTo(models.section, {foreignKey: 'studSectionId'})
                student.hasMany(models.fee, {foreignKey:"feesStudentId"})
                student.hasMany(models.result, {foreignKey:"resStudentId"})
            }
        }
    });
    return student;
};