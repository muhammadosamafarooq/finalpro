var express = require('express');
var router = express.Router();
var admin=require("../controller/admin")
var announcement=require("../controller/announcement")
var campus=require("../controller/campus")
var course=require("../controller/course")
var course2section=require("../controller/course2section")
var exam=require("../controller/exam")
var fees=require("../controller/fees")
var login=require("../controller/login")
var result=require("../controller/result")
var section=require("../controller/section")
var staff=require("../controller/staff")
var staff2campus=require("../controller/staff2campus")
var student=require("../controller/student")
var teacher=require("../controller/teacher")
var teacher2campus=require("../controller/teacher2campus")
var middleware=require("./../middlewares/tokenValidation")
var teacher2course=require("../controller/teacher2course")
//router.use(cors(corsOptions))

router.post('/loginAdmin',login._admin)
router.post('/loginStaff',login._staff)
router.post('/loginStudent',login._student)
router.post('/loginTeacher',login._teacher)

router.get("/students",middleware.checkToken,student.getStudents)
router.get("/students/:id",middleware.checkToken,student.getStudent)
router.post("/students/:sectionName/:classes",middleware.checkToken,student.createStudent)
router.put("/students",middleware.checkToken,student.AdminOrStaffUpdateStudent)
router.delete("/students/:id",middleware.checkToken,student.deleteStudent)

router.get("/teachers/:id",middleware.checkToken,teacher.getTeacher)
router.get("/teachers/:firstName/:lastName",teacher.getTeacherByName)
router.get("/teachers",middleware.checkToken,teacher.getTeacherAll)
router.post("/teachers",middleware.checkToken,teacher.createTeacher)
router.put("/teachers/:id",middleware.checkToken,teacher.AdminOrStaffUpdateteacher)
router.delete("/teachers/:id",middleware.checkToken,teacher.deleteTeacher)

router.get("/staff",middleware.checkToken,staff.getStaffAll)
router.get("/staff/:id",middleware.checkToken,staff.getStaff)
router.post("/staff",middleware.checkToken,staff.createStaff)
router.put("/staff/:id",middleware.checkToken,staff.AdminUpdateStaff)
router.delete("/staff/:id",middleware.checkToken,staff.deleteStaff)

router.get("/admins",middleware.checkToken,admin.getAdmins)
router.get("/admins/:id",middleware.checkToken,admin.getAdmin)
router.post("/admins",middleware.checkToken,admin.createAdmin)
router.put("/admins/:id",middleware.checkToken,admin.AdminupdateAdmin)
router.delete("/admins/:id",middleware.checkToken,admin.deleteAdmin)

router.delete("/course2section/:courseName/:sectionName/:class/:secClass",middleware.checkToken,course2section.deleteCourse2Section)
router.post("/course2section",middleware.checkToken,course2section.addCourse2Section)

router.delete("/staff2campus/:campusId/:staffId",middleware.checkToken,staff2campus.deleteStaff2Campus)
router.post("/staff2campus",middleware.checkToken,staff2campus.addStaff2Campus)

router.delete("/teacher2campus/:campusId/:teacherId",middleware.checkToken,teacher2campus.deleteTeacher2Campus)
router.post("/teacher2campus",middleware.checkToken,teacher2campus.addTeacher2Campus)

router.get("/exams/:sectionId/:courseId",middleware.checkToken,exam.getExam)
router.get("/examsBySection/:sectionId",middleware.checkToken,exam.getExamBySection)
router.post("/exams",middleware.checkToken,exam.createExam)
router.put("/exams/:sectionId/:courseId",middleware.checkToken,exam.updateExam)
router.delete("/exams/:sectionId/:courseId",middleware.checkToken,exam.deleteExam)


router.delete("/results/:studentId/:type/:year/:courseId",middleware.checkToken,result.deleteResult)
router.get("/results/:studentId/:type/:year/:courseId",middleware.checkToken,result.getResult)
router.get("/results/:studentId/:type/:year",middleware.checkToken,result.getResults)
router.post("/results",middleware.checkToken,result.createResult)
router.put("/results/:studentId/:courseId/:type/:year",middleware.checkToken,result.updateResult)

router.delete("/fees/:studentId/:type/:detail",middleware.checkToken,fees.deleteFee)
router.get("/fees/:studentId/:type/:detail",middleware.checkToken,fees.getFee)
router.post("/fees",middleware.checkToken,fees.createFee)
router.put("/fees/:studentId/:type/:detail",middleware.checkToken,fees.updateFees)

router.get("/announcements/:id",announcement.getAnnouncementByCampus)
router.post("/announcements",middleware.checkToken,announcement.createAnnouncement)
router.delete("/announcements/:campusName",announcement.deleteAnnouncement)

router.delete("/campuses/:name",middleware.checkToken,campus.deleteCampus)
router.get("/campuses/:name",campus.getCampusByName)
router.get("/campuses",campus.getCamp)
router.post("/campuses",middleware.checkToken,campus.createCampus)
router.put("/campuses/:id",middleware.checkToken,campus.updateCampus)


router.delete("/sections/:campusName/:sectionName/:class",middleware.checkToken,section.deleteSection)
router.get("/sections/:campusName/:sectionName/:class",middleware.checkToken,section.getSection)
router.get("/sections",middleware.checkToken,section.getSectionAll)
router.post("/sections",middleware.checkToken,section.createSection)
router.put("/sections/:campusName/:sectionName/:class",middleware.checkToken,section.updateSection)

router.delete("/courses/:courseName/:class",middleware.checkToken,course.deleteCourse)
router.get("/courses/:courseName/:class",course.getCourseByNameandClass)
router.get("/courses",course.getCourse)
router.post("/courses",middleware.checkToken,course.createCourse)
router.put("/courses/:courseName/:class",middleware.checkToken,course.updateCourse)

router.delete("/teacher2course/:courseName/:class/:teacherId",middleware.checkToken,teacher2course.deleteTeacher2Course)
router.post("/teacher2course",middleware.checkToken,teacher2course.addTeacher2Course)


module.exports = router;
