'use strict';
const UIDGenerator = require('uid-generator')
const uidgenerator = new UIDGenerator(32, UIDGenerator.BASE62);
var helpers=require("./../helpers")

module.exports = {
    up: function (queryInterface, Sequelize) {


        return queryInterface.bulkInsert('students', [{
            firstName: "Idrees",
            lastName: "Shabbir",
            age: 30,
            contact: "0336",
            address: "karachi",
            email: "student@school.com",
            password: "1234",
            gender: "male",
            image: "new image",
            dateOfJoin: new Date(),
            studSectionId: 1

        },
            {
                firstName: "Afzal",
                lastName: "Hassan",
                age: 30,
                contact: "0336",
                address: "karachi",
                email: "student@school.com",
                password: "1234",
                gender: "male",
                image: "new image",
                dateOfJoin: new Date(),
                studSectionId: 2

            }
        ], {});

    },

    down: function (queryInterface, Sequelize) {

    }
};
