'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {


        return queryInterface.bulkInsert('courses', [{
            name: "maths",
            syllabus: "oxford",
            class: "6",
            crTeacherID:1

        },{
            name: "science",
            syllabus: "oxford",
            class: "6",
            crTeacherID:2
        }], {});

    },

    down: function (queryInterface, Sequelize) {

    }
};
