'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {


        return queryInterface.bulkInsert('fees', [{
            type: "Monthly",
            detail: "jan",
            dueDate: new Date(),
            feesStudentId:1

        },
            {
                type: "Misc",
                detail: "picnic fee",
                status: "paid",
                dateOfPayment: new Date(),
                dueDate: new Date()
            }], {});

    },

    down: function (queryInterface, Sequelize) {

    }
};
