'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {


        return queryInterface.bulkInsert('campuses', [
            {
                name: "NazimabadCampus",
                location: "khi",
                address: "khi",
                image: "myImage",
                adminId:1
            },
            {
                name: "HighwayCampus",
                location: "khi",
                address: "khi",
                image: "myImage",
                adminId:2

            }
        ], {});

    },

    down: function (queryInterface, Sequelize) {

    }
};
