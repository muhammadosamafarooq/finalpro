'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {


        return queryInterface.bulkInsert('results', [{
            marks: 80,
            grade: "A",
            type: "annual",
            year: new Date().getFullYear(),
            resStudentId:1,
            resCourseId:1

        },{
            marks: 80,
            grade: "A",
            type: "annual",
            year: new Date().getFullYear(),
            resStudentId:2,
            resCourseId:2
        }], {});

    },

    down: function (queryInterface, Sequelize) {

    }
};
