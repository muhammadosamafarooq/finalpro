'use strict';
const UIDGenerator = require('uid-generator')
const uidgenerator = new UIDGenerator(32, UIDGenerator.BASE62);
var helpers=require("./../helpers")

let t_salt=helpers.getSalt()

module.exports = {
    up: function (queryInterface, Sequelize) {


        return queryInterface.bulkInsert('staffs', [
            {
                firstName: "Mohammad",
                lastName: "Jawed",
                age: 30,
                contact: "0336",
                address: "karachi",
                email: "teacher@school.com",
                password: "1234",
                gender: "male",
                image: "new image",
                salary:"10000",
                dateOfJoin: new Date(),

            },
            {
                firstName: "Mehboob",
                lastName: "Alam",
                age: 30,
                contact: "0336",
                address: "karachi",
                email: "teacher@school.com",
                password: "1234",
                gender: "male",
                image: "new image",
                salary:"10000",
                dateOfJoin: new Date(),

            }

        ], {});

    },

    down: function (queryInterface, Sequelize) {

    }
};
