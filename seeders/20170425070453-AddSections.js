'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {


        return queryInterface.bulkInsert('sections', [{
            name: "A",
            class: 6,
            secCampusId:1,
            secTeacherId:1

        },
            {
                name: "B",
                class: 6,
                secCampusId:2,
                secTeacherId:2
            }], {});

    },

    down: function (queryInterface, Sequelize) {

    }
};
