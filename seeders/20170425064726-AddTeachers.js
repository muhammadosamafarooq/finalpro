'use strict';

var helpers=require("./../helpers")

module.exports = {
  up: function (queryInterface, Sequelize) {



      return queryInterface.bulkInsert('teachers', [{
          firstName: "Osama",
          lastName: "Ayaz",
          age: 30,
          contact: "0336",
          address: "karachi",
          email: "teacher@school.com",
          password: "1234",
          gender: "male",
          image: "new image",
          salary:"10000",
          dateOfJoin: new Date(),
          qualification: "MSC"
      },
          {
              firstName: "Zain",
              lastName: "Abbas",
              age: 30,
              contact: "0336",
              address: "karachi",
              email: "teacher@school.com",
              password: "1234",
              gender: "male",
              image: "new image",
              salary:"10000",
              dateOfJoin: new Date(),
              qualification: "MSC"
          }], {});
    },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
